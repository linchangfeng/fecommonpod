//
//  FEAlertController.h
//  OnePass
//
//  Created by Tracy on 14/9/25.
//  Copyright (c) 2014年 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FEAlertController;
typedef void (^FEAlertControllerHandler)(FEAlertController *c, NSString *buttonTitle);

@interface FEAlertController : NSObject

@property (nonatomic, strong) id internal;
@property (nonatomic) CGRect sourceRect;

- (id)initWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray*)buttonTitles cancelButtonTitle:(NSString*)cancelButtonTitle destructiveButtonTitle:(NSString*)destructiveButtonTitle preferredStyle:(UIAlertControllerStyle)preferredStyle handler:(void (^)(FEAlertController *c, NSString *buttonTitle))handler;

- (void)showInController:(UIViewController *)controller animated:(BOOL)animated completion:(void (^)(void))completion;

- (UITextField*)textFieldAtIndex:(NSUInteger)index;
- (void)dismiss;
- (void)simulateClickButtonWithTitle:(NSString*)buttonTitle;

@end
