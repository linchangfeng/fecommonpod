//
//  FECommonPod.h
//  Pods
//
//  Created by flowdev on 8/31/16.
//
//

#ifndef FECommonPod_h
#define FECommonPod_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FEUtil.h"
#import "FEInternal.h"

#define kUtil [FEUtil sharedInstance]

#endif /* FECommonPod_h */
