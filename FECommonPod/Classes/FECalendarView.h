//
//  CalendarView.h
//  Account
//
//  Created by Tracy on 1/11/16.
//  Copyright © 2016 Tracy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FECalendarViewDelegate;


//
@interface FECalendarView : UIView

@property (weak) id<FECalendarViewDelegate> delegate;

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDateComponents *currentMonthComponents;

@property (nonatomic, strong) UIColor *monthTitleColor;
@property (nonatomic, strong) UIColor *todayButtonTitleColor;

@property (nonatomic, strong) UIColor *weekTextColor;
@property (nonatomic, strong) UIColor *dayTextColor;
@property (nonatomic, strong) UIColor *dayFadeoutTextColor;
@property (nonatomic, strong) UIColor *selectedDayTextColor;
@property (nonatomic, strong) UIColor *selectedDayBackgroundColor;

@end


//
@protocol FECalendarViewDelegate <NSObject>

@optional
- (void)calendarView:(FECalendarView*)calendarView didSelectDate:(NSDate*)date;
- (void)calendarView:(FECalendarView *)calendarView didJumpToToday:(NSDate*)today;

@end