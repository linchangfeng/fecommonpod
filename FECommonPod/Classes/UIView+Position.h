//
//  UIView+Position.h
//
//  Created by Tyler Neylon on 3/19/10.
//  Copyright 2010 Bynomial.
//
//  These properties enable repositioning of a UIView
//  by changing any single coordinate or size parameter.
//  Don't forget that you can also use UIView's autoresizing
//  mask to achieve some automatic layout.
//
//  Sample usage:
//
//   myView.x += 10;  // Move 10 points right.
//   [myView addCenteredSubview:aSubView];
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIView (Position)

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;

// Setting these modifies the origin but not the size.
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat bottom;

@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

@property (nonatomic, readonly) CGFloat halfWidth;
@property (nonatomic, readonly) CGFloat halfHeight;

@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;

// Methods for centering.
- (void)addCenteredSubview:(UIView *)subview;
- (void)moveToCenterOfSuperview;

@end
