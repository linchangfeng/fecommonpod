//
//  FEUtil_Macro.h
//  FECommon
//
//  Created by Tracy on 15/9/15.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#ifndef FECommon_FEUtil_Macro_h
#define FECommon_FEUtil_Macro_h

#define kFM [NSFileManager defaultManager]
#define kCalendar [NSCalendar currentCalendar]
#define kICloudURL [kFM URLForUbiquityContainerIdentifier:nil]
#define kUbiquitousStore [NSUbiquitousKeyValueStore defaultStore]

#define FEIsPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define FEStringEqual(a, b) ([a isEqualToString:b])
#define FENumberEqual(a, b) ([a isEqualToNumber:b])
#define FEStringWithFormat(format, ...) [NSString stringWithFormat:format, ##__VA_ARGS__]
#define FEImageNamed(name) [UIImage imageNamed:name]
#define FEStatusBarHeight() [UIApplication sharedApplication].statusBarFrame.size.height
#define FEHexRGBA(hex, a) [UIColor colorWithRed:(hex>>16)/255.0 green:((hex&0x00FF00)>>8)/255.0 blue:(hex&0x0000FF)/255.0 alpha:a]

#define FEUserDefaultsValueForKey(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]
#define FEUserDefaultsStringForKey(key) [[NSUserDefaults standardUserDefaults] stringForKey:key]
#define FEUserDefaultsBoolForKey(key) [[NSUserDefaults standardUserDefaults] boolForKey:key]
#define FEUserDefaultsIntegerForKey(key) [[NSUserDefaults standardUserDefaults] integerForKey:key]
#define FEUserDefaultsFloatForKey(key) [[NSUserDefaults standardUserDefaults] floatForKey:key]
#define FESetUserDefaults(key, obj) [[NSUserDefaults standardUserDefaults] setValue:obj forKey:key]

#define FEScreenPoint3_5 CGSizeMake(320, 480)
#define FEScreenPoint4_0 CGSizeMake(320, 568)
#define FEScreenPoint4_7 CGSizeMake(375, 667)
#define FEScreenPoint5_5 CGSizeMake(414, 736)
#define FEScreenPoint7_9 CGSizeMake(768, 1024)
#define FEScreenPoint9_7 CGSizeMake(768, 1024)

#endif
