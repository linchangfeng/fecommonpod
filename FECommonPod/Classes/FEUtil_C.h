//
//  FEUtilC.h
//  Accounts
//
//  Created by Tracy on 15/9/15.
//  Copyright (c) 2015年 Tracy. All rights reserved.
//

#import <Foundation/Foundation.h>

void FEAddObserver(NSObject *observer, NSString *name, SEL action);
void FERemoveObserver(NSObject *observer);
void FERemoveObserver2(NSObject *observer, NSString *name);
void FEPostNotification(NSString *name, NSObject *object, NSDictionary *userInfo);

NSData* FEArchiveObject(id obj);
id FEUnarchiveData(NSData* data);

CGFloat FENavBarHeight(void);
CGFloat FEToolbarHeight(void);

CGFloat CGRectGetX(CGRect rect);
CGFloat CGRectGetY(CGRect rect);
CGFloat CGRectGetRight(CGRect rect);
CGFloat CGRectGetBottom(CGRect rect);

// Transform3D
CATransform3D FECATransform3DMakePerspective(CGPoint center, float disZ);
CATransform3D FECATransform3DPerspect(CATransform3D t, CGPoint center, float disZ);

//
BOOL FEIsNumber(id obj);
BOOL FEIsArray(id obj);
BOOL FEIsDictionary(id obj);

//
void exchangeMethodImplementations(Class cls, SEL sel1, SEL sel2);
void setTimeout(NSTimeInterval delay, void (^aBlock)(void));
