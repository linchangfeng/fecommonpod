#
# Be sure to run `pod lib lint FECommonPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FECommonPod'
  s.version          = '0.1.0'
  s.summary          = 'Private FECommonPod.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Private Pod.
                       DESC

  s.homepage         = 'https://bitbucket.org/linchangfeng/fecommonpod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Tracy' => 'tracy@flowever.net' }
  s.source           = { :git => 'git@flowever.net:FECommonPod', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'FECommonPod/Classes/**/*'
  s.resources = 'FECommonPod/Assets/**/*'
  
#   s.resource_bundles = {
#     'FECommonPod' => ['FECommonPod/Assets/*.png']
#   }

   s.public_header_files = 'FECommonPod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
