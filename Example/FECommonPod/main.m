//
//  main.m
//  FECommonPod
//
//  Created by linchangfeng on 08/31/2016.
//  Copyright (c) 2016 linchangfeng. All rights reserved.
//

@import UIKit;
#import "FEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FEAppDelegate class]));
    }
}
