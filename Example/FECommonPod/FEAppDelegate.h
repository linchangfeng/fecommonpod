//
//  FEAppDelegate.h
//  FECommonPod
//
//  Created by linchangfeng on 08/31/2016.
//  Copyright (c) 2016 linchangfeng. All rights reserved.
//

@import UIKit;

@interface FEAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
